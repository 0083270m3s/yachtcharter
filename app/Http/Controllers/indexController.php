<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class indexController extends Controller
{
    public function showIndex( Request $request )
    {

        $param = [
            'options' => $request->input( 'options' )
        ];

        // проверим не истекла ли сессия
//        if ( !$request->session()->has( 'currentUser.id' ) ) {
//            return redirect('/auth');
//        }

        //
//        $agent = new Agent();
//        if ( $agent->isMobile() ) {
//            $param[ 'platforma' ] = "mobile";
//            return view( 'indexMobile' )->with( $param );
//        } else {
//            $param[ 'platforma' ] = "desktop";
//            $param[ 'userRole' ] = $request->session()->get('currentUser.userRole');
//            $param[ 'userAlias' ] = $request->session()->get('currentUser.userAlias');
//            $param[ 'userId' ] = $request->session()->get('currentUser.id');
//            return view( 'indexDesktop' )->with( $param );
//        }

        clock($param);

        return view( 'welcome' )->with( $param );
    }
}
