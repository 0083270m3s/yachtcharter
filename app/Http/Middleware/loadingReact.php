<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

/**
 * Class loadingReact
 *
 * @package App\Http\Middleware
 */
class loadingReact
{

    public function handle(Request $request, Closure $next)
    {
        $test = Storage::disk('cra')->get('/build/asset-manifest.json');
        $result = json_decode($test, true);

        //clock($result['entrypoints']);

        $prop = array();

        foreach ($result['entrypoints'] as $key => $val) {
            if (Str::contains($val, 'css')) {
                $prop['options']['style'][] = $val;
            } else {
                $prop['options']['js'][] = $val;
            }
        }//END FOREACH

        $request->merge($prop);

        clock($request);

        return $next($request);
    }
}
