//_REACT_Import_
import React, {useCallback, useEffect, useMemo, useState} from "react";
//import { AnimatePresence, motion, useAnimation } from "framer-motion";
import PropTypes from "prop-types";
//import { connect } from 'react-redux';

import {
    LineChartOutlined
} from '@ant-design/icons';

import {
    PageHeader,
    Row,
    Col,
    Divider,
    Select,
    Space,
    List,
    Typography,
    Tag,
    Button,
    Checkbox, Table, Drawer,
} from 'antd';

import {Page, Frame} from "framer";
import OrderForm from "./components/OrderForm";
import WelcomScreen from "./components/WelcomScreen";
import OrderScreen from "./components/OrderScreen";
import {fakeDataRepositoryImages} from "./Service/fakeDataRepository";
import {preloadImg} from "./Service/preLoadingData";
import SliderScreen1 from "./components/Slider/SliderScreen1";
import SliderScreen2 from "./components/Slider/SliderScreen2";


const {Option} = Select;
const {Text, Title, Paragraph} = Typography;

const App = () => {

    //__REACT_State__
    const [state, setState] = useState({
        frontSlider: 0,
        showDrawer: false,
    })

    //__REACT_Method__
    const handleDragEnd = useCallback((event, info) => {

        //обноружим паразитные клики и случайные слайды
        if (info.velocity.x === 0) {
            return false
        }
        if (info.velocity.x < 30 && info.velocity.x > -30) {
            return false
        }

        let nextSlider = state.frontSlider

        // условия определения дипазона слада
        if (info.offset.x < -30) {
            nextSlider = (nextSlider > 2) ? state.frontSlider : state.frontSlider + 1
        }
        if (info.offset.x > 30) {
            nextSlider = (nextSlider < 2) ? state.frontSlider : state.frontSlider - 1
        }

        setState({
            ...state,
            frontSlider: nextSlider,
        })

    }, [state])

    //__REACT_Method__
    const onChangeSlide = useCallback(() => {
        setState({
            ...state,
            frontSlider: 2,
        })
    }, [state])

    //__REACT_Method__
    const onDrawerOpen = useCallback(() => {
        setState({
            ...state,
            showDrawer: true
        })
    }, [state])

    //__REACT_useEffect__
    useEffect(() => {
        //let cancelFlag = false;
        //if (!cancelFlag) {}
        //return () => { cancelFlag = true}

        /*Сделаем прелоадер всех img */
        //preloadImg(fakeDataRepositoryImages,['img'])

    }, [])


    //__REACT_Method__
    const onScrollToOrderScreen = useCallback(() => {
        setState({
            ...state,
            frontSlider: 3,
        })
    }, [state])

    //__ATTENTION__
    //console.table(state);
    // console.log(URLPATH);

    //__REACT_return__
    return (
        <>
            <Page
                currentPage={state.frontSlider }
                defaultEffect={"cube"}
                width={'100vw'}
                height={'100vh'}
                onChangePage={(current, previous) => {
                    console.log(current, previous)
                    setState({
                        ...state,
                        frontSlider: current,
                    })
                }}
            >

                <Frame size={100}>
                    <WelcomScreen
                        //key={state.frontSlider}
                        keyTest={state.frontSlider}
                        onScrollToOrderScreen={onScrollToOrderScreen}
                    />
                </Frame>

                <Frame size={100}>
                    <SliderScreen1
                        keyTest={state.frontSlider}
                        onScrollToOrderScreen={onScrollToOrderScreen}
                    />
                </Frame>

                <Frame size={100}>
                    <SliderScreen2
                        keyTest={state.frontSlider}
                        onScrollToOrderScreen={onScrollToOrderScreen}
                    />
                </Frame>

                <Frame size={100}>
                    <OrderScreen
                        keyTest={state.frontSlider}
                        onDrawerOpen={onDrawerOpen}
                    />
                </Frame>

            </Page>

            <Drawer
                // title="Basic Drawer"
                width={'80vw'}
                placement="right"
                closable={true}
                onClose={() => setState({
                    ...state,
                    showDrawer: false
                })
                }
                visible={state.showDrawer}
            >
                {
                    (state.showDrawer)&&
                    <OrderForm/>
                }
            </Drawer>
        </>
        // <>
        //     <h1>Hello asa 123</h1>
        // </>
    )

} //END CLASS


export default React.memo(App);
