import { urlPath } from './urlPath'

export let fakeDataRepositoryImages = [
    {
        // 'img': '/images/welcomScreen.jpg',
        'img': urlPath('/images/welcomScreen.jpg', 'BI'),
        'name': '0',
        'id': 0,
    },
    {
        // 'img': '/images/sliderScreen1.jpg',
        'img': urlPath('/images/sliderScreen1.jpg', 'BI'),
        'name': '1',
        'id': 1,
    },
    {
        // 'img': '/images/sliderScreen2.jpg',
        'img': urlPath('/images/sliderScreen2.jpg', 'BI'),
        'name': '2',
        'id': 2,
    },
    {
        // 'img': '/images/orderScreen1.jpg',
        'img': urlPath('/images/orderScreen1.jpg', 'BI'),
        'name': '3',
        'id': 3,
    },
]
