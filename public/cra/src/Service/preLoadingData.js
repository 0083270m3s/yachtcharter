/*
Метод прекешинга изображений

Принимает массив с обьектами в котором есть ключи с url img
которые надо подкешировать
например:
[{
    'img': '/img/collectionBox/1.jpg',
    'imgFull': '/img/collectionBox/1full.jpg',
    'name': 'Топ 10 велопрогулок 1',
    'id': 1,
  },
  {
    'img': '/img/collectionBox/2.jpg',
    'imgFull': '/img/collectionBox/2full.jpg',
    'name': 'Топ 10 пешком 2',
    'id': 2,
  },
]

Принимает массив с ключами , котореы надо найти в data и выбрать url
и начать кешировать
например:
target ['img','imgFull']


*/
export const preloadImg = (data = [], target = []) => {

  //__STEP__
  target.map((targetValue, targetIndex) => {

    data.map((dataValue, dataIndex) => {
      if (targetValue in dataValue) {

        /*==== ВАРИАНТ 1 ===*/
        const linkEl = document.createElement('link')
        linkEl.setAttribute('rel', 'preload')
        linkEl.setAttribute('href', dataValue[ targetValue ])
        linkEl.setAttribute('as', 'image')
        document.head.appendChild(linkEl)
        /*=======*/

      }
    })

  })

  //__STEP__
  console.log('Кеширование завершено');
}
