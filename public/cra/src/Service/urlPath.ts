export const urlPath = (basePath: string, mode?: "BI"): string => {

    /*

    Метод принимает url ресурса basePath
    и возвращает мутацию в зависимости от среды сборки (dev | prod)

    Учтонения:
    basePath:
        может начинатся с '/' или без него - всеравно вернется корректный url

    mode:
        необзяательный флаг корректировки
        BI
            флаг используется если надо обернуть в url(XXX)
            например в значении css background-images
    */

    let finalPath = (basePath[0] === '/')
        ? basePath.slice(1)
        : basePath

    if (process.env.NODE_ENV === 'production') {
        switch (mode) {
            case 'BI':
                return `url(/cra/public/${finalPath})`;
                break;
            default:
                return `/cra/public/${finalPath}`;
                break;
        }

    } else {
        switch (mode) {
            case 'BI':
                return `url(/${finalPath})`;
                break;
            default:
                return `/${finalPath}`
                break;
        }

    }
}
