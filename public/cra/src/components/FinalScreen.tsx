//_REACT_Import_
import React, { useCallback, useEffect, useMemo, useState } from "react";
//import { AnimatePresence, motion, useAnimation } from "framer-motion";
import PropTypes from "prop-types";
//import { connect } from 'react-redux';

import {
    LineChartOutlined
} from '@ant-design/icons';

import {
    PageHeader,
    Row,
    Col,
    Divider,
    Select,
    Space,
    List,
    Typography,
    Tag,
    Button,
    Checkbox, Table,
} from 'antd';

const { Option } = Select;
const { Text, Title, Paragraph } = Typography;

const FinalScreen = (  ) => {

    //__REACT_State__
    // const [state, setState] = useState( {} );


    //__REACT_return__
    return (
        <>
            <Row>
                <Col span={24}>
                    <Title level={5} className={'cls_fw'}>
                        Спасибо,<br/> ваш заказ принят.
                    </Title>
                </Col>
                <Col span={24} className={'cls_mt50px'}>
                    <Title level={5} className={'cls_fw'}>
                        В течении 5 минут наш менеджер Вам перезвонит.
                    </Title>
                </Col>
            </Row>
        </>
    )

} //END CLASS


/*
delay: <void> (e) => {}
*/

/*
FinalScreen.propTypes = {
    PPPPPPP: PropTypes.func.isRequired,
};
*/

export default React.memo(FinalScreen);
//export default FinalScreen;

/*
const areEqual = (prev, next) =>

{
    areEqual возвращает true, если пропсы равны,
    и значение false, если пропсы не равны
    Если true = повторного рендера не будет
}
*/

