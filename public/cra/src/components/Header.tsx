//_REACT_Import_
import React, { useCallback, useEffect, useMemo, useState } from "react";
//import { AnimatePresence, motion, useAnimation } from "framer-motion";
import PropTypes from "prop-types";
//import { connect } from 'react-redux';

import {
    LineChartOutlined
} from '@ant-design/icons';

import {
    PageHeader,
    Row,
    Col,
    Divider,
    Select,
    Space,
    List,
    Typography,
    Tag,
    Button,
    Checkbox, Table,
} from 'antd';
import HookDownUpSlide from "./frameHooks/HookDownUpSlide";

const { Option } = Select;
const { Text, Title, Paragraph } = Typography;

//__TS INTERFACE__
interface IHeaderProps {
    // onButtonChangeSlider: () => void,
    // onDrawerOpen: () => void,
    onScrollToOrderScreen: () => void,
}

const Header = ( props: IHeaderProps ) => {

    //__REACT_State__
    // const [state, setState] = useState( {} );


    //__REACT_return__
    return (
        <HookDownUpSlide>
            <div
                style={{
                    backgroundColor: '#30303099',
                    height: '20px',
                    width: '80%',
                    left: '10%',
                    position: 'absolute',
                    top: '40px',
                    borderRadius: '30px',
                    display: 'flex',
                    justifyContent: 'space-around',
                    alignItems: 'baseline',
                }}
            >
                <HookDownUpSlide delay={0.2}>
                    <Button
                        style={{
                            marginTop: '-4px',
                        }}
                        className={'pulse cls_br30px'}
                        onClick={props.onScrollToOrderScreen}
                    >
                        Заказать
                    </Button>
                </HookDownUpSlide>
                <HookDownUpSlide delay={0.4}>
                    <Title
                        level={4}
                        className={'cls_c1'}
                           style={{
                               marginTop: '-8px',
                           }}
                    >
                        VAINEMOINEN
                    </Title>
                </HookDownUpSlide>
            </div>
        </HookDownUpSlide>
    )

} //END CLASS


/*
delay: <void> (e) => {}
*/

/*
Header.propTypes = {
    PPPPPPP: PropTypes.func.isRequired,
};
*/

export default React.memo(Header);
//export default Header;

/*
const areEqual = (prev, next) =>

{
    areEqual возвращает true, если пропсы равны,
    и значение false, если пропсы не равны
    Если true = повторного рендера не будет
}
*/

