//_REACT_Import_
import React, {useCallback, useEffect, useMemo, useState} from "react";
//import { AnimatePresence, motion, useAnimation } from "framer-motion";
import PropTypes from "prop-types";
//import { connect } from 'react-redux';

import {
    LineChartOutlined
} from '@ant-design/icons';

import {
    PageHeader,
    Row,
    Col,
    Divider,
    Select,
    Space,
    List,
    Typography,
    Tag,
    Button,
    Checkbox, Table, notification,
} from 'antd';
import ScreenKeyboard from "./controlComponents/ScreenKeyboard";
import HookDownUpSlide from "./frameHooks/HookDownUpSlide";
import FinalScreen from "./FinalScreen";
import {urlPath} from "../Service/urlPath";

const {Option} = Select;
const {Text, Title, Paragraph} = Typography;

//__TS INTERFACE__
interface IOrderFormState {
    /*Имя клиента*/
    clientName: string | undefined,
    /*Телефон клиента*/
    clientPhone: string | undefined,
    /*Статус офрмления заказать*/
    orderSuccessful: boolean,
    loadingState: boolean,
}

//__REACT_Component__
const OrderForm = () => {

    //__REACT_State__
    const [state, setState] = useState<IOrderFormState>({
        clientName: undefined,
        clientPhone: undefined,
        orderSuccessful: false,
        loadingState: false,

    });

    //__REACT_Method__
    const onOrder = useCallback(() => {
        //__VALIDATION__
        if (state.clientName === undefined
            || state.clientName.length === 0) {

            notification.warning({
                message: 'Имя'
            });
            return false;
        }

        if (state.clientName === undefined || state.clientName.length === 0) {

            notification.warning({
                message: 'Имя'
            });
            return false;
        }

        if (state.clientPhone === undefined || state.clientPhone.length < 10) {
            notification.warning({
                message: 'Телефон'
            });
            return false;
        }


        //__STEP__
        setState({
            ...state,
            loadingState: true
        })

        //__STEP__
        setTimeout(() => {

            setState({
                ...state,
                orderSuccessful: true,
                loadingState: false,
            })

        }, 1000)

    }, [state])


    //__REACT_return__
    return (
        <>

            {
                /*Ордер принят*/
                (state.orderSuccessful) &&
                <FinalScreen/>
            }

            {
                /*Офрмление ордера*/
                (!state.orderSuccessful) &&
                <>
                    <Row>
                        <Col span={24}>
                            <HookDownUpSlide>
                                <Title level={5} className={'cls_c3'}>Оформить заказ</Title>
                            </HookDownUpSlide>
                        </Col>
                    </Row>
                    <Row>
                        <Col span={24}>
                            <Space direction={'vertical'}>
                                <HookDownUpSlide delay={0.2}>
                                    <Title level={5} className={'cls_fw'}>ИМЯ</Title>
                                    <input
                                        type="text"
                                        className={'cls_ipt1'}
                                        onChange={(e) => setState({
                                            ...state,
                                            clientName: e.target.value
                                        })}
                                    />
                                </HookDownUpSlide>
                            </Space>
                        </Col>

                        <Col span={24} className={'cls_mt20px'}>
                            <Space direction={'vertical'}>
                                <HookDownUpSlide delay={0.4}>
                                    <Title level={5} className={'cls_fw'}>ТЕЛЕФОН</Title>
                                    <Space direction={'horizontal'}>
                                        <Title level={4}>+7</Title>
                                        <input
                                            type="text"
                                            maxLength={10}
                                            className={'cls_ipt2 cls_w90P'}
                                            onChange={(e) => setState({
                                                ...state,
                                                clientPhone: e.target.value
                                            })}
                                        />
                                    </Space>
                                </HookDownUpSlide>
                            </Space>
                        </Col>

                        <Col span={24} className={'cls_tac cls_mt50px'}>
                            <HookDownUpSlide delay={0.8}>
                                <Button
                                    type={'primary'}
                                    className={'cls_br30px'}
                                    onClick={onOrder}
                                    loading={state.loadingState}
                                >
                                    Заказать
                                </Button>
                            </HookDownUpSlide>
                        </Col>
                    </Row>

                    <Divider/>

                    <Row>
                        <Col span={24} className={'cls_tac'}>
                            <HookDownUpSlide delay={1}>
                                <Title level={5} className={'cls_c3'}>или через мессанжеры</Title>
                            </HookDownUpSlide>
                        </Col>
                    </Row>
                    <Row>
                        <Col span={12} className={'cls_tac'}>
                            <HookDownUpSlide delay={1.2}>
                                <a href="https://wa.me/79384540155" target="_blank"
                                   rel="noreferrer">
                                    {/*<img src="/images/whatsApp3.png" className={'cls_w60px'}/>*/}
                                    <img src={urlPath('/images/whatsApp3.png')}
                                         className={'cls_w60px'}/>
                                </a>
                            </HookDownUpSlide>
                        </Col>
                        <Col span={12} className={'cls_tac'}>
                            <HookDownUpSlide delay={1.4}>
                                <a href="https://t.me/radpriemzayavok3" target="_blank"
                                   rel="noreferrer">
                                    {/*<img src="/images/Telegramlogo2.png" className={'cls_w60px'}/>*/}
                                    <img src={urlPath('/images/Telegramlogo2.png')}
                                         className={'cls_w60px'}/>
                                </a>
                            </HookDownUpSlide>
                        </Col>
                    </Row>
                </>
            }


        </>
    )

}
//END CLASS

export default React.memo(OrderForm);

