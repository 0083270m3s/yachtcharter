//_REACT_Import_
import React, {useCallback, useEffect, useMemo, useState} from "react";
//import { AnimatePresence, motion, useAnimation } from "framer-motion";
import PropTypes from "prop-types";
//import { connect } from 'react-redux';

import {
    CheckCircleOutlined
} from '@ant-design/icons';

import {
    PageHeader,
    Row,
    Col,
    Divider,
    Select,
    Space,
    List,
    Typography,
    Tag,
    Button,
    Checkbox, Table,
} from 'antd';
import HookDownUpSlide from "./frameHooks/HookDownUpSlide";
import {urlPath} from "../Service/urlPath";

const {Option} = Select;
const {Text, Title, Paragraph} = Typography;


//__TS INTERFACE__
interface IOrderScreenProps {
    onDrawerOpen: () => void,
    keyTest: number,
}

//__REACT_Component__
const OrderScreen = (props: IOrderScreenProps) => {

    //__REACT_State__
    // const [state, setState] = useState( {} );

    console.table('OrderScreen render')

    //__REACT_return__
    return (
        <div
            key={Math.floor(Math.random() * (1000 - 1 + 1)) + 1}
            style={{
                backgroundImage: urlPath('/images/orderScreen1.jpg', 'BI'),
                height: '100%',
                width: '100%',
                backgroundSize: 'cover',
            }}

            className={'filterSepia'}
        >

            <div
                style={{
                    backgroundColor: '#00000061',
                    height: '75vh',
                    width: '85%',
                    position: 'absolute',
                    top: '7vh',
                    left: '7%',
                    borderRadius: '30px',
                    padding: '30px 0 30px 0',
                }}

                className={'filterSepia'}
            >
                {/*======================*/}
                <Row className={'cls_mt20px'}>
                    <Col span={24} className={'cls_tac'}>
                        <HookDownUpSlide>
                            <Row>
                                <Col span={4} className={'cls_tae'}>
                                    <CheckCircleOutlined className={'cls_fs30px cls_c1'}/>
                                </Col>
                                <Col span={14} offset={2} className={'cls_tas'}>
                                    <Text className={'cls_fs16px cls_c1'}>с животными</Text>
                                </Col>
                            </Row>
                        </HookDownUpSlide>
                        <HookDownUpSlide delay={0.2}>
                            <Row>
                                <Col span={4} className={'cls_tae'}>
                                    <CheckCircleOutlined className={'cls_fs30px cls_c1'}/>
                                </Col>
                                <Col span={14} offset={2} className={'cls_tas'}>
                                    <Text className={'cls_fs16px cls_c1'}>с детьми</Text>
                                </Col>
                            </Row>
                        </HookDownUpSlide>
                        <HookDownUpSlide delay={0.4}>
                            <Row>
                                <Col span={4} className={'cls_tae'}>
                                    <CheckCircleOutlined className={'cls_fs30px cls_c1'}/>
                                </Col>
                                <Col span={14} offset={2} className={'cls_tas'}>
                                    <Text className={'cls_fs16px cls_c1'}>свой алкоголь</Text>
                                </Col>
                            </Row>
                        </HookDownUpSlide>
                    </Col>
                </Row>
                {/*======================*/}
                <Row>
                    <Col span={24} className={'cls_tac'}>
                        <HookDownUpSlide delay={0.6}>
                            <Divider className={'cls_bc4'}/>
                            <Space direction={'horizontal'}>
                                <Title className={'cls_fs40px cls_c1 cls_fw'}>
                                    20 000
                                </Title>
                                <Title level={5} className={'cls_fs16px cls_c1'}>
                                    руб.
                                </Title>
                            </Space>
                        </HookDownUpSlide>
                    </Col>
                    <Col span={24} className={'cls_tac'}>
                        <HookDownUpSlide delay={0.8}>
                            <Text className={'cls_fs16px cls_c1'}>
                                за час прогулки
                            </Text>
                        </HookDownUpSlide>
                    </Col>
                </Row>
                {/*======================*/}
                <Row className={'cls_mt50px'}>
                    <Col span={24} className={'cls_tac'}>
                        <HookDownUpSlide delay={1}>
                            <Button
                                style={{
                                    width: '50%',
                                    height: '50px',
                                }}
                                className={'pulse cls_br30px'}
                                onClick={props.onDrawerOpen}
                            >
                                Заказать
                            </Button>
                        </HookDownUpSlide>
                    </Col>
                </Row>
                {/*======================*/}
                {/*======================*/}
            </div>


        </div>
    )

} //END CLASS


const areEqual = (prev: any, next: any) => {
    /*Если true = повторного рендера не будет*/
    if (next.keyTest === 3) {
        return false
    } else {
        return true
    }

}

export default React.memo(OrderScreen, areEqual);
//export default OrderScreen;


