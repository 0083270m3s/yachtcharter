//_REACT_Import_
import React, {useCallback, useEffect, useMemo, useState} from "react";
//import { AnimatePresence, motion, useAnimation } from "framer-motion";
import PropTypes from "prop-types";
//import { connect } from 'react-redux';

import {
    DragOutlined
} from '@ant-design/icons';

import {
    PageHeader,
    Row,
    Col,
    Divider,
    Select,
    Space,
    List,
    Typography,
    Tag,
    Button,
    Checkbox, Table,
} from 'antd';
import HookDownUpSlide from "../frameHooks/HookDownUpSlide";
import Header from "../Header";
import {urlPath} from "../../Service/urlPath";

const {Option} = Select;
const {Text, Title, Paragraph} = Typography;

//__TS INTERFACE__
interface ISliderScreen2Props {
    keyTest: number,
    onScrollToOrderScreen: () => void,
}

const SliderScreen2 = (props: ISliderScreen2Props) => {

    //__REACT_State__
    /*const [state, setState] = useState( {} );*/

    console.table('SliderScreen2 render')

    //__REACT_return__
    return (

        <div
            key={Math.floor(Math.random() * (1000 - 1 + 1)) + 1}
            style={{
                backgroundImage: urlPath('/images/sliderScreen2.jpg', 'BI'),
                height: '100%',
                width: '100%',
                backgroundSize: 'cover',
            }}

            className={'filterSepia'}
        >
            {/*TOP DOM*/}
            <Header
                onScrollToOrderScreen={props.onScrollToOrderScreen}
            />

            {/*BOTTOM DOM*/}
            <HookDownUpSlide>
                <div
                    className={'cls_sliderScreenOverlay'}
                >
                    <div
                        className={'cls_sliderScreenOverlayCorrection'}
                    >
                        <HookDownUpSlide>
                            <Title level={3} className={'cls_c1 cls_m0px'}>
                                Место позагорать
                            </Title>
                        </HookDownUpSlide>

                        <HookDownUpSlide delay={0.2}>
                            <Title level={5} className={'cls_c1 cls_m0px'}>
                                До черной корочки
                            </Title>
                        </HookDownUpSlide>
                    </div>
                </div>
            </HookDownUpSlide>
        </div>
    )

} //END CLASS


const areEqual = (prev: any, next: any) =>
{
    /*Если true = повторного рендера не будет*/
    if ( next.keyTest === 2 ) {
        return false
    } else {
        return true
    }

}
export default React.memo(SliderScreen2, areEqual);
//export default SliderScreen2;




