//_REACT_Import_
import React, {useCallback, useEffect, useMemo, useState} from "react";
//import { AnimatePresence, motion, useAnimation } from "framer-motion";
import PropTypes from "prop-types";
//import { connect } from 'react-redux';

import {
    UpOutlined
} from '@ant-design/icons';

import {
    PageHeader,
    Row,
    Col,
    Divider,
    Select,
    Space,
    List,
    Typography,
    Tag,
    Button,
    Checkbox, Table,
} from 'antd';
import {Frame} from "framer";
import HookDownUpSlide from "./frameHooks/HookDownUpSlide";

const {Option} = Select;
const {Text, Title, Paragraph} = Typography;

//__TS INTERFACE__
interface ISliderScreenProps {
    onDrawerOpen: () => void,
    keyTest: number,
}

//__REACT_Component__
const SliderScreen = (props: ISliderScreenProps) => {

    //__REACT_State__
    const [state, setState] = useState({
        frontSlider: 1,
    })

    //__REACT_Method__
    const handleDragEnd = useCallback((event, info) => {

        //обноружим паразитные клики и случайные слайды
        if (info.velocity.y === 0) {
            return false
        }
        if (info.velocity.y < 30 && info.velocity.y > -30) {
            return false
        }

        let nextSlider = state.frontSlider

        // условия определения дипазона слада
        if (info.offset.y < -30) {
            nextSlider = (nextSlider > 2) ? state.frontSlider : state.frontSlider + 1
        }
        if (info.offset.y > 30) {
            nextSlider = (nextSlider < 2) ? state.frontSlider : state.frontSlider - 1
        }

        setState({
            ...state,
            frontSlider: nextSlider,
        })

    }, [state])

    //__REACT_Method__
    const onChangeSlide = useCallback(() => {
        let nextSlider = state.frontSlider;

        if (state.frontSlider + 1 > 2) {
            nextSlider = 1;
        } else {
            nextSlider = nextSlider + 1
        }

        setState({
            ...state,
            frontSlider: nextSlider,
        })
    }, [state])

    //__ATTENTION__
    //console.table(state);
    console.table('SliderScreen render')


    //__REACT_return__
    return (
        <div
            key={Math.floor(Math.random() * (1000 - 1 + 1)) + 1}
        >
            <Frame
                //background={( !isREFUELING && !isENDED ) && { src: props.garage[ state.frontSlider ].img }}
                //shadow="rgb(223 217 217) 7px 9px 6px 0px"

                //key={props.garage[ state.frontSlider ].id}
                key={state.frontSlider}
                className={'cls_sliderFramev1'}
                //drag={'y'}
                dragElastic={0.8}
                dragConstraints={{
                    right: 0,
                    left: 0,
                    bottom: 0,
                    top: 0,

                }}
                onDragEnd={handleDragEnd}
                //onClick={() => props.callback(props.garage[ state.frontSlider ])}

                initial={{
                    // y: 50,
                    opacity: 0,
                }}

                animate={{
                    // y: 0,
                    opacity: 1,
                }}

                transition={{
                    // type: 'spring',
                    opacity: {},
                    // y: {duration: 0.5}
                }}
            >

                {/*TOP DOM*/}
                <HookDownUpSlide>
                    <div
                        style={{
                            backgroundColor: '#19191999',
                            height: '40px',
                            width: '80%',
                            left: '10%',
                            position: 'absolute',
                            top: '20px',
                            borderRadius: '30px',
                            display: 'flex',
                            justifyContent: 'space-around',
                            alignItems: 'baseline',
                        }}
                    >
                        <HookDownUpSlide delay={0.2}>
                            <Button
                                style={{
                                    height: '40px',
                                }}
                                className={'pulse cls_br30px'}
                                onClick={props.onDrawerOpen}
                            >
                                Заказать
                            </Button>
                        </HookDownUpSlide>
                        <HookDownUpSlide delay={0.4}>
                            <Title level={4} className={'cls_c1 cls_m0px'}>
                                Компания
                            </Title>
                        </HookDownUpSlide>
                    </div>
                </HookDownUpSlide>

                {/*========= SLIDER 1 =================*/}
                {
                    (state.frontSlider === 1) &&
                    <div
                      style={{
                          backgroundImage: 'url(/images/sliderScreen1.jpg)',
                          height: '100%',
                          width: '100%',
                          backgroundSize: 'cover',
                      }}

                      className={'filterSepia'}
                    >
                        {/*BOTTOM DOM*/}
                      <HookDownUpSlide>
                        <div
                          style={{
                              backgroundColor: '#19191999',
                              height: '200px',
                              width: '80%',
                              left: '10%',
                              position: 'absolute',
                              top: '60vh',
                              borderRadius: '30px',
                          }}
                        >
                          <div
                            style={{
                                height: '100%',
                                display: 'flex',
                                flexDirection: 'column',
                                justifyContent: 'center',
                                width: '100%',
                                alignItems: 'center',
                            }}
                          >
                            <HookDownUpSlide>
                              <Title level={3} className={'cls_c1 cls_m0px'}>
                                Белая яхта
                              </Title>
                            </HookDownUpSlide>

                            <HookDownUpSlide delay={0.2}>
                              <Title level={5} className={'cls_c1 cls_m0px'}>
                                Такая белая что ослепнуть
                              </Title>
                            </HookDownUpSlide>

                            <HookDownUpSlide delay={0.4}>
                                {/*<UpOutlined*/}
                                {/*  onClick={onChangeSlide}*/}
                                {/*  className={'cls_c1 cls_fs40px cls_mt20px cls_br30px pulse'}*/}
                                {/*/>*/}
                              <Button
                                onClick={onChangeSlide}
                                className={'cls_br30px cls_mt20px cls_btnFix'}
                              >
                                <UpOutlined className={'cls_fs40px cls_c1'}/>
                              </Button>
                            </HookDownUpSlide>
                          </div>
                        </div>
                      </HookDownUpSlide>
                    </div>
                }
                {/*========= SLIDER 2 =================*/}
                {
                    (state.frontSlider === 2) &&
                    <div
                      style={{
                          backgroundImage: 'url(/images/sliderScreen2.jpg)',
                          height: '100%',
                          width: '100%',
                          backgroundSize: 'cover',
                      }}

                      className={'filterSepia'}
                    >
                        {/*BOTTOM DOM*/}
                      <HookDownUpSlide>
                        <div
                          style={{
                              backgroundColor: '#19191999',
                              height: '200px',
                              width: '80%',
                              left: '10%',
                              position: 'absolute',
                              top: '60vh',
                              borderRadius: '30px',
                          }}
                        >
                          <div
                            style={{
                                height: '100%',
                                display: 'flex',
                                flexDirection: 'column',
                                justifyContent: 'center',
                                width: '100%',
                                alignItems: 'center',
                            }}
                          >
                            <HookDownUpSlide>
                              <Title level={3} className={'cls_c1 cls_m0px'}>
                                Место позагорать
                              </Title>
                            </HookDownUpSlide>

                            <HookDownUpSlide delay={0.2}>
                              <Title level={5} className={'cls_c1 cls_m0px'}>
                                До черной корочки
                              </Title>
                            </HookDownUpSlide>

                            <HookDownUpSlide delay={0.4}>
                              <Button
                                onClick={onChangeSlide}
                                className={'cls_br30px cls_mt20px cls_btnFix'}
                              >
                                <UpOutlined className={'cls_fs40px cls_c1'}/>
                              </Button>
                            </HookDownUpSlide>
                          </div>
                        </div>
                      </HookDownUpSlide>
                    </div>
                }
                {/*==========================*/}
            </Frame>
        </div>
    )

} //END CLASS


const areEqual = (prev: any, next: any) =>
{
    /*Если true = повторного рендера не будет*/
    if ( next.keyTest === 1 ) {
        console.info('SliderScreen keyTest == 1');
        return false
    } else {
        return true
    }

}

export default React.memo(SliderScreen, areEqual);
//export default SliderScreen;


