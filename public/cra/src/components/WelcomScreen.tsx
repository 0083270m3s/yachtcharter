//_REACT_Import_
import React, {useCallback, useEffect, useMemo, useState} from "react";
//import { AnimatePresence, motion, useAnimation } from "framer-motion";
import PropTypes from "prop-types";
//import { connect } from 'react-redux';

import {
    DragOutlined
} from '@ant-design/icons';

import {
    PageHeader,
    Row,
    Col,
    Divider,
    Select,
    Space,
    List,
    Typography,
    Tag,
    Button,
    Checkbox, Table,
} from 'antd';
import HookDownUpSlide from "./frameHooks/HookDownUpSlide";
import Header from "./Header";
import {urlPath} from "../Service/urlPath";

const {Option} = Select;
const {Text, Title, Paragraph} = Typography;

//__TS INTERFACE__
interface IWelcomScreenProps {
    onScrollToOrderScreen: () => void,
    keyTest: number,
}

const WelcomScreen = (props: IWelcomScreenProps) => {

    //__REACT_State__
    /*const [state, setState] = useState( {} );*/

    console.table('WelcomScreen render')

    //__REACT_return__
    return (

        <div
            key={Math.floor(Math.random() * (1000 - 1 + 1)) + 1}
            style={{
                backgroundImage: urlPath('/images/welcomScreen.jpg', 'BI'),
                height: '100%',
                width: '100%',
                backgroundSize: 'cover',
            }}

            className={'filterSepia'}
        >
            {/*TOP DOM*/}
            <Header
                onScrollToOrderScreen={props.onScrollToOrderScreen}
            />

            {/*BOTTOM DOM*/}
            <HookDownUpSlide>
                <div
                    style={{
                        backgroundColor: '#004e6f99',
                        height: '200px',
                        width: '80%',
                        left: '10%',
                        position: 'absolute',
                        top: '55vh',
                        borderRadius: '30px',
                    }}
                >
                    <div
                        style={{
                            height: '100%',
                            display: 'flex',
                            flexDirection: 'column',
                            justifyContent: 'center',
                            width: '100%',
                            alignItems: 'center',
                        }}
                    >
                        <HookDownUpSlide>
                            <Title level={3} className={'cls_c1 cls_m0px'}>
                                Let's start
                            </Title>
                        </HookDownUpSlide>

                        <HookDownUpSlide delay={0.2}>
                            <Title level={3} className={'cls_c1 cls_m0px'}>
                                You travel
                            </Title>
                        </HookDownUpSlide>
                    </div>
                </div>
            </HookDownUpSlide>
        </div>
    )

} //END CLASS


/*
delay: <void> (e) => {}
*/

/*
WelcomScreen.propTypes = {
    PPPPPPP: PropTypes.func.isRequired,
};
*/

const areEqual = (prev: any, next: any) =>
{
    /*Если true = повторного рендера не будет*/
    if ( next.keyTest === 0 ) {
        return false
    } else {
        return true
    }

}
export default React.memo(WelcomScreen, areEqual);
//export default WelcomScreen;




