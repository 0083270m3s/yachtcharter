//_REACT_Import_
import React, {useCallback, useEffect, useMemo, useState} from 'react'
//import { AnimatePresence, motion, useAnimation } from "framer-motion";
import PropTypes from 'prop-types'
//import { connect } from 'react-redux';

import {
    CaretLeftOutlined, CheckOutlined,
    LineChartOutlined,
} from '@ant-design/icons'

import {
    PageHeader,
    Row,
    Col,
    Divider,
    Select,
    Space,
    List,
    Typography,
    Tag,
    Button,
    Checkbox, Table,
} from 'antd'

//import HookDownUpSlide from '../frameHooks/HookDownUpSlide'

const {Option} = Select
const {Text, Title} = Typography

//__TS INTERFACE__
interface IScreenKeyboardState {
    number: number[],
}

//__TS INTERFACE__
interface IScreenKeyboardProps {
    /*Возвращает результат в виде массива цифр*/
    callback: (e: number[]) => void,
    /*Ограничение по длинне результирующего массива*/
    numberOfDigits: number,
    /*Скрыть кнопку Enter*/
    hideEnter?: boolean
}

const ScreenKeyboard = (props: IScreenKeyboardProps) => {

    //__REACT_State__
    const [state, setState] = useState<IScreenKeyboardState>({
        number: [],
    })

    //__REACT_Method__
    /*e => Цифра нажатая на экранной клавиатуре || backspace || enter*/

    const onChange = useCallback((e) => {

        let count = props.numberOfDigits
        let number = state.number

        //__STEP__
        if (e === 'backspace') {
            number.pop()
        }

        if (e === 'enter') {
            //props.callback('enter')
            return false
        }

        if (number.length >= count) {
            return false
        }

        if (e !== 'backspace' && e !== 'enter') {
            number.push(e)
        }

        //__STEP__
        setState({
            ...state,
            number: number,
        })

        props.callback(number)

    }, [props])

    //__REACT_return__
    return (
        <Row>
            <Col span={20} offset={2}>


                {/*Клавиши 1-9*/}
                {
                    [[1, 2, 3], [4, 5, 6], [7, 8, 9]].map((currentValue, index) =>

                        // <HookDownUpSlide>
                            <Row className={'cls_mt20px'} key={index}>
                                {
                                    currentValue.map((currentValue2, index2) =>

                                        <Col span={8} className={'cls_tac'}>
                                            <Button
                                                type="dashed"
                                                shape="circle"
                                                size={'large'}
                                                onClick={() => onChange(currentValue2)}
                                            >
                                                <Title level={4}>{currentValue2}</Title>
                                            </Button>
                                        </Col>,
                                    )
                                }
                            </Row>
                        // </HookDownUpSlide>,
                    )
                }

                {/*Особый нижний ряд*/}
                {/*<HookDownUpSlide delay={0.1}>*/}
                    <Row className={'cls_mt20px'}>
                        <Col span={8} className={'cls_tac'}>
                            <Button
                                type="dashed"
                                shape="circle"
                                size={'large'}
                                onClick={() => onChange('backspace')}
                            >
                                <CaretLeftOutlined/>
                            </Button>
                        </Col>

                        <Col span={8} className={'cls_tac'}>
                            <Button
                                type="dashed"
                                shape="circle"
                                size={'large'}
                                onClick={() => onChange(0)}
                            >
                                <Title level={4}>0</Title>
                            </Button>
                        </Col>

                        {
                            (props.hideEnter !== true)&&
                            <Col span={8} className={'cls_tac'}>
                              <Button
                                type="primary"
                                shape="circle"
                                size={'large'}
                                onClick={() => onChange('enter')}
                              >
                                <CheckOutlined/>
                              </Button>
                            </Col>
                        }

                    </Row>
                {/*</HookDownUpSlide>*/}


            </Col>

        </Row>

    )

} //END CLASS

/*

*/

// ScreenKeyboard.propTypes = {
//     callback: PropTypes.func.isRequired,
//     numberOfDigits: PropTypes.number.isRequired,
//     hideEnter: PropTypes.bool,
// }

// const areEqual = (prev, next) => {
//   return true
// }

export default React.memo(ScreenKeyboard)
//export default ScreenKeyboard;
