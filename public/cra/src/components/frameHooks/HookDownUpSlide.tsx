//_REACT_Import_
import React, {useEffect, useMemo, useState} from "react";
import {motion} from "framer-motion";
import PropTypes from "prop-types";

//__TS INTERFACE__
interface IHookDownUpSlideProps {
    /*
    значение задержки framer delay
    если не delay не передан - задержка будет 0
    */
    delay?: number,
    /*
    значение продолжительности framer duration
    если не duration не передан - задержка будет default (1.5)
    */
    duration?: number,
    children?: any,
}

const HookDownUpSlide = (props: IHookDownUpSlideProps) => {

    //__REACT_State__
    // const [state, setState] = useState( {} );


    //__REACT_return__
    return (
        <motion.div
            initial={{
                y: 50,
                opacity: 0
            }}

            animate={{
                y: 0,
                opacity: 1
            }}

            transition={{
                duration: (props.duration) ? props.duration : 1.5,
                delay: (props.delay) && props.delay
            }}
        >
            {props.children}
        </motion.div>
    )

} //END CLASS


/*
delay: <int> значение задержки framer delay
    если не delay не передан - задержка будет 0

duration: <int> значение продолжительности framer duration
    если не duration не передан - задержка будет default (1.5)
*/

HookDownUpSlide.propTypes = {
    delay: PropTypes.number,
    duration: PropTypes.number,
};

// const areEqual = (prev, next) => {
//     //logic equal
// }

export default React.memo(HookDownUpSlide);
//export default HookDownUpSlide;

