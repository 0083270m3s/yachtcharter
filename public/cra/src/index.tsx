import React from 'react';
import ReactDOM from 'react-dom';
import './App.css';
// import * as Sentry from "@sentry/react";
// import { Integrations } from "@sentry/tracing";
import App from './App';
import reportWebVitals from './reportWebVitals';


// let RELEASE = '0.1.0';
// Sentry.init({
//     dsn: "http://29e1005b0f514125a6d3b90816f01f14@192.168.254.128:9000/4",
//     integrations: [new Integrations.BrowserTracing()],
//
//     // Set tracesSampleRate to 1.0 to capture 100%
//     // of transactions for performance monitoring.
//     // We recommend adjusting this value in production
//     tracesSampleRate: 1.0,
//     release: RELEASE,
// });


ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
