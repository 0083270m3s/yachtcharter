export interface INWORKERSECTION {
    /*
    Флаг секции от которой поступил запрос
    URGENT_ACTIVE_ORDERS - Срочные активные заявки
    MANUALCREATIONLATTERTO - ручное создание заявки
*/
    inWorkerSection: "URGENT_ACTIVE_ORDERS" | "MANUALCREATIONLATTERTO" | "TASKFORTHEDAY";
    /*
   что конкретно запрещено
   CLIENT_REPLIED - btn Клиент ответил
   CLIENT_NOT_REPLIED - btn Клиент не ответил
   CLIENT_AGREED = btn Клиент согласился
   CLIENT_REFUSED = btn Клиент отказался
   CLIENT_CALLBACK = btn Клиент попросил перезвонить
   CLIENT_NOT_ANSWERING = btn Клиент Не берет трубку
   CLIENT_NUMBER_NOT_AVAILABLE = bnt Клиент Номер не доступен

   */
    forbidden: "CLIENT_REPLIED"
        | "CLIENT_NOT_REPLIED"
        | "CLIENT_AGREED"
        | "CLIENT_REFUSED"
        | "CLIENT_CALLBACK"
        | "CLIENT_NOT_ANSWERING"
        | "CLIENT_NUMBER_NOT_AVAILABLE"
}

export interface ISCREENCARD {
    /*
    Константы для InWorkOrderHOC
    указывают какой экран - компонент требуется отрисовать:
    SCREENCARD1 ... все экраны индексируются порядковыми номерами
    STOPCARD - экран , когда сценарий закончен и требуется освободить InWorkerOrder
    */
    screenCard: "SCREENCARD1"
        | "SCREENCARD2"
        | "SCREENCARD3"
        | "SCREENCARD4"
        | "SCREENCARD5"
        | "SCREENCARD6"
        | "STOPCARD"
}

export interface IACCESSRIGHTS {
    /*
    Константы которые сервер возвращает при запросе данных
    Указывают на поведение компонентов
       ALLOWED: доступ разрешен
       DENIED: запрещен
       ERROR: ошибка в работе
       undefined: статст не определен
    */
    access: "ALLOWED" | "DENIED" | "ERROR" | undefined,
}

export interface ISCREENCARD3TIMESTATUS {
    /*
    Константы которые сервер возвращает при запросе данных
    Указывают на поведение компонентов
    */
    timeStatus: "NOTIME" | "WITHTIME" | undefined
}

export interface ISCREENCARD6VARIANT {
    /*
    Константы которые сервер возвращает при запросе данных
    Указывают на поведение компонентов
      AGREETOSENDDOC: "Заявка перемещается в секцию  Срочные / Ожидающие на указанное время",
      UPLOADDOC: "Откроется форма загрузки документов",
      COMINGOFFICE: "Откроется форма заполнения заявки",
    */
    variant: "AGREETOSENDDOC" | "UPLOADDOC" | "COMINGOFFICE" | undefined
}


export interface IFAKEDATA {
    /*
    Общий статус ответа
    ВНИМАНИЕ: если access !== ALLOWED то status сервером присвоется ERROR
    */
    status: "CORRECT" | "ERROR",
    /*Статус доступа*/
    access: IACCESSRIGHTS['access'],
    /*Если
    status === ERROR в date будет расшифровка исключения
    status === CORRECT в date будет необходимая деменная информация
    ( например следующий скрин )
    */
    data: ISCREENCARD['screenCard'],

}

export interface IFAKEASSOC {
    CLIENT_AGREED : ISCREENCARD['screenCard'],
    CLIENT_REFUSED: ISCREENCARD['screenCard'],
    CLIENT_CALLBACK: ISCREENCARD['screenCard'],
    CLIENT_NOT_ANSWERING: ISCREENCARD['screenCard'],
    CLIENT_NUMBER_NOT_AVAILABLE: ISCREENCARD['screenCard'],
    CLIENT_REPLIED: ISCREENCARD['screenCard'],
    CLIENT_NOT_REPLIED: ISCREENCARD['screenCard'],
}

export interface ITASKFORTHEDAY {
    /*
    Классификация задачи в секции задачи-на-день
    URGENT - задача срочной секции
    PENDING - задача ожидающей секции
    */
    relevance: 'URGENT' | 'PENDING',
    /*
    Режим задачи:
    LATTERTO флаг задачи из заявки
    */
    mode: 'LATTERTO'
}
