<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    @foreach ( $options['style'] as $item)
        <link href="/cra/build/{{ $item }}" rel="stylesheet">
    @endforeach

{{--    <link href="/frontend/build/static/css/2.b4e56faa.chunk.css" rel="stylesheet">--}}
{{--    <link href="/frontend/build/static/css/main.02af5444.chunk.css" rel="stylesheet">--}}
</head>
<body class="antialiased">

<div id="root" class="cls_overXhidden"></div>

@foreach ( $options['js'] as $item)
    <link href="/cra/build/{{ $item }}" rel="stylesheet">
    <script src="/cra/build/{{ $item }}"></script>
@endforeach

{{--<script src="/frontend/build/static/js/runtime-main.54b90f07.js"></script>--}}
{{--<script src="/frontend/build/static/js/2.11d8851a.chunk.js"></script>--}}
{{--<script src="/frontend/build/static/js/main.d08e2c88.chunk.js"></script>--}}

</body>
</html>
